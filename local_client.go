package media

import (
	"context"
	"io"
	"os"
	"path/filepath"

	"gitlab.com/knopkalab/go/errors"
	"gitlab.com/knopkalab/go/utils"
)

// localClient storage
type localClient struct {
	*Config
}

func openLocalClient(conf *Config) (minClient, error) {
	return &localClient{Config: conf}, nil
}

func (c *localClient) Put(
	ctx context.Context, category, filename string,
	size int64, body io.Reader) (link string, err error) {

	dir, path, link := c.getDirPathLink(filename, category)

	if err = os.MkdirAll(dir, os.ModePerm); err != nil {
		return "", errors.Stack(err)
	}
	f, err := os.Create(path)
	if err != nil {
		return "", errors.Stack(err)
	}
	defer f.Close()

	_, err = io.Copy(f, body)
	return link, errors.Stack(err)
}

func (c *localClient) getDirPathLink(filename, category string) (dir, path, link string) {
	filename = filepath.Base(filename)
	token := utils.RandString(c.RandPrefixLen)

	dir, link = c.LocalDir, c.LocalURL
	if category != "" {
		dir = filepath.Join(dir, category)
		link += category + "/"
	}
	dir = filepath.Join(dir, token)
	path = filepath.Join(dir, filename)
	link += token + "/" + filename
	return
}
