package media

import (
	"bytes"
	"context"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"mime/multipart"
	"os"

	"gitlab.com/knopkalab/go/errors"
	"gitlab.com/knopkalab/go/utils"
	"gitlab.com/knopkalab/tracer"
)

type minClient interface {
	Put(ctx context.Context, category, filename string, size int64, body io.Reader) (link string, err error)
}

type clientWrapper struct {
	minClient
	uploadCtx context.Context
}

func newClientWrapper(ctx context.Context, client minClient) *clientWrapper {
	return &clientWrapper{
		minClient: client,
		uploadCtx: tracer.StartSpanContext(ctx, "media: uploads"),
	}
}

func (s *clientWrapper) put(
	spanCtx tracer.SpanContext, category, filename string,
	size int64, body io.Reader) (link string, err error) {

	defer spanCtx.Finish()

	spanCtx.Debug().
		Str("file_category", category).
		Str("file_name", filename).
		Int64("file_size", size).
		Msg("upload file begin")

	link, err = s.minClient.Put(spanCtx, category, filename, size, body)

	if err == nil {
		spanCtx.Debug().Str("remote_link", link).Msgf("success: %s", link)
	} else {
		spanCtx.Error(err).Msgf("upload file `%s`/`%s` (%d bytes) failed", category, filename, size)
	}
	return
}

func (s *clientWrapper) startSpanContext(ctx context.Context, span string) tracer.SpanContext {
	if tracer.Check(ctx) {
		return tracer.StartSpanContext(ctx, span)
	}
	return tracer.StartSpanContext(s.uploadCtx, span)
}

func (s *clientWrapper) Put(
	ctx context.Context, category, filename string,
	size int64, body io.Reader) (link string, err error) {

	spanCtx := s.startSpanContext(ctx, "media: upload body")

	return s.put(spanCtx, category, filename, size, body)
}

func (s *clientWrapper) PutFile(ctx context.Context, category, filepath string) (link string, err error) {
	spanCtx := s.startSpanContext(ctx, "media: upload file")

	f, err := os.Open(filepath)
	if err != nil {
		err = errors.Stack(err)
		spanCtx.Error(err).Msgf("open local file `%s` failed", filepath)
		spanCtx.Finish()
		return "", err
	}
	defer f.Close()
	finfo, err := f.Stat()
	if err != nil {
		err = errors.Stack(err)
		spanCtx.Error(err).Msgf("open local file `%s` failed", filepath)
		spanCtx.Finish()
		return "", errors.Stack(err)
	}

	return s.put(spanCtx, category, finfo.Name(), finfo.Size(), f)
}

func (s *clientWrapper) PutData(
	ctx context.Context, category, filename string, data []byte) (link string, err error) {

	spanCtx := s.startSpanContext(ctx, "media: upload bytes")

	return s.put(spanCtx, category, filename, int64(len(data)), bytes.NewReader(data))
}

type ImageType int

const (
	ImageJPG ImageType = iota
	ImagePNG
)

func (s *clientWrapper) PutImage(
	ctx context.Context, category, filename string, img image.Image, typ ImageType) (link string, err error) {

	spanCtx := s.startSpanContext(ctx, "media: upload image")

	var buf bytes.Buffer
	switch typ {
	case ImagePNG:
		err = errors.Stack(png.Encode(&buf, img))
		filename = utils.ChangeFileExtension(filename, "png")
	default:
		err = errors.Stack(jpeg.Encode(&buf, img, nil))
		filename = utils.ChangeFileExtension(filename, "jpg")
	}
	if err != nil {
		spanCtx.Error(err).Msgf("read image file `%s` failed", filename)
		spanCtx.Finish()
		return "", err
	}

	return s.put(spanCtx, category, filename, int64(buf.Len()), &buf)
}

func (s *clientWrapper) PutMultipartThumbnail(
	ctx context.Context, category string,
	file *multipart.FileHeader, typ ImageType, maxPixelSize uint) (link string, err error) {

	spanCtx := s.startSpanContext(ctx, "media: upload multipart thumbnail file")

	f, err := file.Open()
	if err != nil {
		err = errors.Stack(err)
		spanCtx.Error(err).Msgf("read multipart file `%s`/`%s` failed", category, file.Filename)
		spanCtx.Finish()
		return "", err
	}
	defer f.Close()

	var imgData []byte
	var filename string
	switch typ {
	case ImagePNG:
		imgData, err = utils.ImagePNGThumbnail(f, file.Filename, maxPixelSize, maxPixelSize)
		err = errors.Stack(err)
		filename = utils.ChangeFileExtension(file.Filename, "png")
	default:
		imgData, err = utils.ImageJPGThumbnail(f, file.Filename, maxPixelSize, maxPixelSize)
		err = errors.Stack(err)
		filename = utils.ChangeFileExtension(file.Filename, "jpg")
	}
	if err != nil {
		spanCtx.Error(err).Msgf("read multipart file `%s`/`%s` failed", category, file.Filename)
		spanCtx.Finish()
		return "", err
	}

	return s.put(spanCtx, category, filename, int64(len(imgData)), bytes.NewReader(imgData))
}
