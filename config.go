package media

import (
	"strings"

	"gitlab.com/knopkalab/go/errors"
)

// Config for local storage or s3 connection
type Config struct {
	RandPrefixLen int `yaml:"rand_prefix_len" desc:"rand file prefix for safe"`

	Local    bool   `yaml:"local" desc:"enable local storage for files"`
	LocalDir string `yaml:"local_dir" desc:"local storage directory"`
	LocalURL string `yaml:"local_url" desc:"target url file prefix"`

	S3AccessKey string `yaml:"s3_access_key" desc:"access key or login or user, for example: 32423_goservice"`
	S3SecretKey string `yaml:"s3_secret_key" desc:"secret key or password"`
	S3AuthToken string `yaml:"s3_auth_token" desc:"optional"`

	S3EndPoint string `yaml:"s3_end_point" desc:"provider api url, for example: s3.selcdn.ru"`
	S3Domain   string `yaml:"s3_domain" desc:"storage domain, for example: https://56756.selcdn.ru"`
	S3Region   string `yaml:"s3_region" desc:"for example: ru-1a"`
	S3Bucket   string `yaml:"s3_bucket" desc:"service bucket root, for example: goservice"`
	S3Insecure bool   `yaml:"s3_insecure"`
}

// ValidAndRepair config
func (c *Config) ValidAndRepair() error {
	if c.RandPrefixLen < 1 {
		c.RandPrefixLen = 64
	}
	if c.Local {
		c.repairLocal()
		return nil
	}
	return c.validAndRepairS3()
}

func (c *Config) repairLocal() {
	if c.LocalDir == "" {
		c.LocalDir = "media"
	}
	if c.LocalURL == "" {
		c.LocalURL = "/media/"
	}
	c.LocalURL = "/" + strings.Trim(c.LocalURL, "/") + "/"
}

func (c *Config) validAndRepairS3() error {
	if c.S3EndPoint == "" {
		c.S3EndPoint = "s3.selcdn.ru"
	}
	if c.S3Region == "" {
		c.S3Region = "ru-1a"
	}
	c.S3Bucket = strings.Trim(strings.TrimSpace(c.S3Bucket), "/")
	switch {
	case c.S3AccessKey == "":
		return errors.New("mediaClient config.s3_access_key no set")
	case c.S3SecretKey == "":
		return errors.New("mediaClient config.s3_secret_key no set")
	case c.S3EndPoint == "":
		return errors.New("mediaClient config.s3_end_point no set")
	case c.S3Domain == "":
		return errors.New("mediaClient config.s3_domain no set")
	case c.S3Region == "":
		return errors.New("mediaClient config.s3_region no set")
	case c.S3Bucket == "":
		return errors.New("mediaClient config.s3_bucket no set")
	}
	c.S3Domain = strings.Trim(c.S3Domain, "/") + "/"
	return nil
}
